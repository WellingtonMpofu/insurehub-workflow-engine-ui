package com.kaributechs.insurehubworkflowengineui.models.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigInteger;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DocumentDTO extends CMISObjectDTO {
    private String category;
    private BigInteger clientId;
    private String clientFirstName;
    private String clientLastName;
    private String clientIdNumber;
    private String processId;
    private String documentUniqueId;
    private String path;
    private String downloadUrl;
    private byte[] data;

    public DocumentDTO(String id, String name, String type, String category, BigInteger clientId, String clientFirstName, String clientLastName, String clientIdNumber, String processId, String documentUniqueId, String path, String downloadUrl, byte[] data) {
        super(id, name, type);
        this.category = category;
        this.clientId = clientId;
        this.clientFirstName = clientFirstName;
        this.clientLastName = clientLastName;
        this.clientIdNumber = clientIdNumber;
        this.processId = processId;
        this.documentUniqueId = documentUniqueId;
        this.path = path;
        this.downloadUrl = downloadUrl;
        this.data = data;
    }
}
