package com.kaributechs.insurehubworkflowengineui.models.dtos.claims;

import lombok.Data;

import java.util.Date;

@Data
public class ClaimDriverDetailsDTO {
    private String driversFirstName;
    private String driversLastName;
    private String driversEmail;
    private String driversPhoneNumber;
    private String driversResidentialStreetAddress;
    private String driversSuburb;
    private String driversResidentialCity;
    private String driversRegion;
    private String driversCountry;
    private String driversZipCode;
    private String driversWorkingStreetAddress;
    private String driversWorkingSuburb;
    private String driversWorkingCity;
    private String driversWorkingRegion;
    private String driversWorkingCountry;
    private String driversWorkingZipCode;
    private String driversLicenseNumber;
    private Date dateOfLicenseIssue;
}
