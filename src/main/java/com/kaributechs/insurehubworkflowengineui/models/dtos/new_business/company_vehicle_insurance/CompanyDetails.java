package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.company_vehicle_insurance;

import lombok.Data;

@Data

public class CompanyDetails {

    private Long id;
    private String companyClass;
    private String companyName;
    private String cin;
    private String registrationNumber;
    private String businessDescription;
}
