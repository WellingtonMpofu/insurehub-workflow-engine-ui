package com.kaributechs.insurehubworkflowengineui.models.dtos.dealer;

import lombok.Data;
import java.math.BigDecimal;


@Data
public class QuotationDTO {

    private Long id;
    private BigDecimal price;
    private String serviceName;

}
