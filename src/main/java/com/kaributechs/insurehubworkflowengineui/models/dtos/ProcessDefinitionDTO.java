package com.kaributechs.insurehubworkflowengineui.models.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProcessDefinitionDTO {
    String id;

    String name;

    String key;

    String description;

    int version;

    String formKey;


}
