package com.kaributechs.insurehubworkflowengineui.models.dtos.dealer;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
public class VehicleDetailsDTO {
    private String vehicleRegistration;
    private String vehicleMake;
    private String vehicleModel;
    private int vehicleYear;
    private String vehicleUsage;
    private Long vehicleMileage;
}
