package com.kaributechs.insurehubworkflowengineui.models.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.kaributechs.insurehubworkflowengineui.models.dtos.enums.TaskStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TaskDTO {


    String id;

    String owner;

    String assignee;

    String name;

    String description;

    Date createdDate;

    Date claimedDate;

    Date dueDate;

    int priority;

    String processDefinitionId;

    String processInstanceId;

    String parentTaskId;

    String formKey;

    Date completedDate;

    Long duration;

    Integer processDefinitionVersion;

    String businessKey;

    boolean isStandalone;

    String taskDefinitionKey;

    private TaskStatus status;
    private String category;
    private String insuranceType;
    private String businessFunction;
    private Map<String,String> assignedTeam;



}
