package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.company_vehicle_insurance;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
public class OtherVehicleUserDetailsDTO {

    private String lastName;
    private String firstName;
    private String salutation;
    private String nationalId;
    private Date dateOfBirth;
    private String emailAddress;
    private String phoneNumber;
    private String gender;
    private String relationshipToBeneficiary;
    private String licenseNumber;
    private String role;
    private String roleDescription;
    private String maritalStatus;
    private String identificationType;

}
