package com.kaributechs.insurehubworkflowengineui.models.dtos.claims;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.Date;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

public class DBWorkflowRequestDTO {
    private boolean acceptDeclaration;
    private Date claimCreationDate;
    private String insuranceType;
    private Long companyId;
    private ClaimProcessDetailsDTO processDetails;
    private ClaimClientDetailsDTO clientDetails;
    private ClaimInsuredDetailsDTO insuredDetails;
    private ClaimVehicleDetailsDTO vehicleDetails;
    private ClaimDriverDetailsDTO driverDetails;
    private ClaimAccidentDetailsDTO accidentDetails;
    private String documentSourceChannel;
}
