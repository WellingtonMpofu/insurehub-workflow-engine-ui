package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.company_vehicle_insurance;

import lombok.Data;

@Data
public class CompanyInsuranceQuestions {

    private Long id;
    private String carInsuranceType;
    private String thirdPartyLiability;
    private String allRisksYesOrNo;
    private String replacementCarYesOrNo;
    private String totalLossYesOrNo;
    private String excessYesOrNo;
    private String periodOfInsuranceTo;
    private String periodOfInsuranceFrom;
    private String maritalStatus;
}
