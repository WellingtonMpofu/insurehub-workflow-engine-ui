package com.kaributechs.insurehubworkflowengineui.models.dtos.claims;

import lombok.Data;

@Data
public class ClaimInsuredDetailsDTO {
    private String insuredId;
    private String insuredSalutation;
    private String insuredFirstName;
    private String insuredLastName;
    private String insuredPhoneNumber;
    private String insuredEmail;
    private String insuredResidentialStreetAddress;
    private String insuredSuburb;
    private String insuredResidentialCity;
    private String insuredRegion;
    private String insuredCountry;
    private String insuredZipCode;
    private String insuredWorkingStreetAddress;
    private String insuredWorkingSuburb;
    private String insuredWorkingCity;
    private String insuredWorkingRegion;
    private String insuredWorkingCountry;
    private String insuredWorkingZipCode;
}
