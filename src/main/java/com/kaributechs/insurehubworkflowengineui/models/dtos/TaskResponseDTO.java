package com.kaributechs.insurehubworkflowengineui.models.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Map;

@NoArgsConstructor
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TaskResponseDTO {
    private TaskDTO task;
    private Map<String, String> variables;

    public TaskResponseDTO(TaskDTO task, Map<String, String> variables) {
        this.task = task;
        this.variables = variables;
    }
}
