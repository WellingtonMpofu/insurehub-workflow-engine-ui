package com.kaributechs.insurehubworkflowengineui.models.dtos.dealer;

import lombok.*;

@Data
public class ServiceDTO {
    private Long id;
    private String name;
}
