package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.funeral_insurance;


import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.ContactDetails;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.SpouseDetails;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class FuneralInsurance {

    private Long id;

    private String processId;


    private FuneralInsuranceMemberDetails memberDetails;


    private ContactDetails contactDetails;


    private SpouseDetails spouse;


    private Set<FuneralInsuranceChildrenDetails> children;


    private Set<FuneralInsuranceExtendedFamilyDetails> extendedFamily;


    private Set<FuneralInsuranceBeneficiaryDetails> beneficiaries;


    private FuneralInsuranceQuestions insuranceQuestions;
}
