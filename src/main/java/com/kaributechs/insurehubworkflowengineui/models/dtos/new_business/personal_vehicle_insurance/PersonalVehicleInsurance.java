package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.personal_vehicle_insurance;

import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.ContactDetails;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.PersonalDetails;
import lombok.Data;

import java.util.Set;

@Data
public class PersonalVehicleInsurance {

    private Long id;
    private String processId;


    private PersonalDetails personalDetails;


    private ContactDetails contactDetails;


    private Set<PersonalVehicleInsuranceVehicleDetails> vehicles;


    private PersonalVehicleInsuranceQuestions insuranceQuestions;

}
