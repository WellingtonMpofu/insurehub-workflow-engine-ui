package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.funeral_insurance;

import lombok.Data;

@Data
public class FuneralInsuranceQuestions {

    private Long id;
    private String benefits_coffin;
    private String benefits_grocery;
    private String benefits_bus;
    private String benefits_repatriation;
    private String benefits_condolenceFee;
    private String insuranceQuestions_excessYesOrNo;
    private String benefits_extendedFamilyBenefits;
    private String insuranceQuestions_periodOfInsuranceFrom;
    private String maritalStatus;
}
