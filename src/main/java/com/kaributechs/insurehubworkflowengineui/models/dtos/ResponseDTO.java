package com.kaributechs.insurehubworkflowengineui.models.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class ResponseDTO {
    String message;

    public ResponseDTO(String message) {
        this.message = message;
    }
}
