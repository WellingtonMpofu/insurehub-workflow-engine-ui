package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.household_insurance;

import lombok.Data;

import java.util.Date;

@Data
public class HouseholdInsuranceExtendedFamilyDetailsDTO {

    private String lastName;
    private String firstName;
    private String salutation;
    private String nationalId;
    private Date dateOfBirth;
    private String emailAddress;
    private String phoneNumber;
    private String gender;
    private String identificationType;
    private String relationshipToBeneficiary;
    private String maritalStatus;



}
