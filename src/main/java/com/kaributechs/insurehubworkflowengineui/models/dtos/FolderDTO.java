package com.kaributechs.insurehubworkflowengineui.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FolderDTO extends CMISObjectDTO{
    private String path;

    public FolderDTO(String id, String name, String type, String path) {
        super(id, name, type);
        this.path = path;
    }
}
