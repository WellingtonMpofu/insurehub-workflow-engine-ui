package com.kaributechs.insurehubworkflowengineui.models.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class AssignTaskDTO {
    String taskId;
    String username;

    public AssignTaskDTO(String taskId, String username) {
        this.taskId = taskId;
        this.username = username;
    }
}
