package com.kaributechs.insurehubworkflowengineui.models.dtos.claims;

import lombok.Data;

import java.util.Map;

@Data
public class ClaimProcessDetailsDTO {
    private String initiator;
    private Map<String,String> team;
    private Double claimAmount;
    private boolean approved;
    private boolean autoAssess;
    private boolean getMoreDetails;
    private boolean getPanelBeater;
    private String claimStatus;
}
