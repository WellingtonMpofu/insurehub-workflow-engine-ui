package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.company_vehicle_insurance;

import lombok.Data;

@Data
public class CompanyDetailsDTO {
    private String companyClass;
    private String companyName;
    private String cin;
    private String registrationNumber;
    private String businessDescription;
}
