package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.medical_insurance;

import lombok.Data;

@Data
public class MedicalInsuranceQuestionsDTO {
    private String insuranceType;
    private String insurancePremiumRange;
    private String paymentFrequency;
    private String insuranceCurrency;
    private String condolenceFee;
    private String excessYesOrNo;
    private String extendedFamilyBenefits;
    private String periodOfInsuranceFrom;
    private String maritalStatus;
}
