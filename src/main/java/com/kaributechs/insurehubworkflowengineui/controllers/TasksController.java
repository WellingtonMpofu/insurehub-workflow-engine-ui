package com.kaributechs.insurehubworkflowengineui.controllers;

import com.kaributechs.insurehubworkflowengineui.models.dtos.*;
import com.kaributechs.insurehubworkflowengineui.models.dtos.claims.CompleteClaimTaskDTO;
import com.kaributechs.insurehubworkflowengineui.services.ICamundaTaskServices;
import com.kaributechs.insurehubworkflowengineui.utilities.Utils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
@Tag(name = "Workflow Process Tasks API endpoints", description = "Endpoints for managing workflow process tasks")
@CrossOrigin
public class TasksController {
    final ICamundaTaskServices camundaTaskServices;
    final Utils utils;

    public TasksController( ICamundaTaskServices camundaTaskServices, Utils utils) {
        this.camundaTaskServices = camundaTaskServices;
        this.utils = utils;
    }

    @RolesAllowed({"admin","user"})
    @GetMapping("tasks/my-tasks")
    @Operation(summary = "Get my tasks")
    public List<TaskDTO> getMyTasks(){
//        utils.getUserAttributes();
        return camundaTaskServices.getMyTasksImp();
    }

    @RolesAllowed("admin")
    @GetMapping("/{processInstanceId}/tasks")
    @Operation(summary = "Get process instance tasks")
    public List<TaskDTO> getProcessTasks(@PathVariable String processInstanceId){
        return camundaTaskServices.getProcessTasksImp(processInstanceId);
    }

    @RolesAllowed({"admin","manager"})
    @GetMapping("/team/tasks")
    @Operation(summary = "Get all tasks ina a process")
    public List<TaskDTO> getAllTasks(){
        return camundaTaskServices.getTeamTasksImp();
    }

    @RolesAllowed("admin")
    @GetMapping("/user-tasks/{username}")
    @Operation(summary = "Get task by username")
    public List<TaskDTO> getTasksByUsername(@PathVariable String username){
        return camundaTaskServices.getTasksByUsernameImp(username);
    }

    @RolesAllowed({"admin","user"})
    @GetMapping("/task/{taskId}")
    @Operation(summary = "Get task by task id")
    public TaskResponseDTO getTaskById(@PathVariable String taskId){
        return camundaTaskServices.getTaskById(taskId);
    }

    @RolesAllowed({"admin","user"})
    @PostMapping("/task/{taskId}/complete")
    @Operation(summary = "Complete task using task id")
    public TaskDTO completeTask(@PathVariable String taskId, @RequestBody CompleteClaimTaskDTO completeClaimTaskDTO){
        return camundaTaskServices.completeTask(taskId, completeClaimTaskDTO);
    }

    @RolesAllowed({"admin","user"})
    @PostMapping("/task/{taskId}/claim")
    @Operation(summary = "Claim a task")
    public ResponseDTO claimTask(@PathVariable String taskId){
        return camundaTaskServices.claimTask(taskId);
    }

    @RolesAllowed("admin")
    @PostMapping("/task/assign")
    @Operation(summary = "Assign a task")
    public ResponseDTO assignTask(@RequestBody AssignTaskDTO assignTaskDTO){
        return camundaTaskServices.assignTask(assignTaskDTO);
    }
}
