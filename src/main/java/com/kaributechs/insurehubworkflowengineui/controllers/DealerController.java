package com.kaributechs.insurehubworkflowengineui.controllers;


import com.kaributechs.insurehubworkflowengineui.models.dtos.RegisterDealerDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.dealer.AddDealerServiceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.dealer.DealerDTO;
import com.kaributechs.insurehubworkflowengineui.services.dealer.IDealerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/dealer")
@Tag(name = "Dealer Process API endpoints", description = "Endpoints for the dealer only")
@Slf4j
public class DealerController {
    private final IDealerService dealerService;

    public DealerController(IDealerService dealerService) {
        this.dealerService = dealerService;
    }


    @PostMapping("/register")
    @Operation(summary = "Register as a dealer")
    public DealerDTO registerDealer(@RequestBody @Valid RegisterDealerDTO registerDealerDTO){
        log.info("Adding Dealer : {}",registerDealerDTO);
        return dealerService.registerDealer(registerDealerDTO);
    }

    @PostMapping("/service/add")
    @Operation(summary = "Add services to your dealer account")
    public DealerDTO addDealerService(@RequestBody @Valid AddDealerServiceDTO addDealerServiceDTO){
        log.info("Adding Dealer Service: {}", addDealerServiceDTO);
        return dealerService.addDealerService(addDealerServiceDTO);
    }

    @GetMapping("/{username}")
    @Operation(summary = "Get dealer by username")
    public DealerDTO getDealerByUsername(@PathVariable String username){
        return dealerService.getDealerByUsername(username);
    }
}
