package com.kaributechs.insurehubworkflowengineui.controllers;


import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.household_insurance.HouseholdInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.household_insurance.HouseholdInsuranceDTO;
import com.kaributechs.insurehubworkflowengineui.services.new_business.IHouseholdInsuranceService;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/household-insurance")
@Tag(name = "Household insurance API endpoints", description = "Endpoints for household insurance only")
@Slf4j
public class HouseholdInsuranceController {
    private final IHouseholdInsuranceService householdInsuranceService;

    public HouseholdInsuranceController(IHouseholdInsuranceService householdInsuranceService) {
        this.householdInsuranceService = householdInsuranceService;
    }

    @PostMapping("/apply")
    @Operation(summary = "Apply for household insurance")
    public ProcessInstanceDTO apply(@RequestBody @Valid HouseholdInsuranceDTO householdInsuranceDTO){
        return householdInsuranceService.apply(householdInsuranceDTO);
    }

    @GetMapping("/get/{processId}")
    @Operation(summary = "Get life insurance details by process id")
    public HouseholdInsurance getInsuranceByProcessId(@PathVariable String processId){
        return householdInsuranceService.getInsuranceByProcessId(processId);
    }
}
