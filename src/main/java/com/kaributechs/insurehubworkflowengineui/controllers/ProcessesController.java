package com.kaributechs.insurehubworkflowengineui.controllers;

import com.kaributechs.insurehubworkflowengineui.models.dtos.*;
import com.kaributechs.insurehubworkflowengineui.models.dtos.claims.RequestClaimProcessVariablesDTO;
import com.kaributechs.insurehubworkflowengineui.services.ICamundaProcessServices;
import com.kaributechs.insurehubworkflowengineui.utilities.Utils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
@Tag(name = "Workflow Processes API endpoints", description = "Endpoints for managing workflow processes")
@CrossOrigin
public class ProcessesController {

    final ICamundaProcessServices camundaProcessServices;
    final Utils utils;

    public ProcessesController( ICamundaProcessServices camundaProcessServices, Utils utils) {
        this.camundaProcessServices = camundaProcessServices;
        this.utils = utils;
    }

    @RolesAllowed("admin")
    @GetMapping("/processes")
    @Operation(summary = "Get all processes")
    public List<ProcessDefinitionDTO> getAllProcesses(){
        utils.getUsername();

        return camundaProcessServices.getAllProcessesImp();
    }

    @RolesAllowed("admin")
    @GetMapping({"/processes/running"})
    @Operation(summary = "Get all running processes")
    public List<ProcessInstanceDTO> getRunningProcesses(){
        return camundaProcessServices.getRunningProcessesImp();
    }
//
    @RolesAllowed("admin")
    @PostMapping("/process/{processId}/suspend")
    @Operation(summary = "Suspend process by process id (claim id)")
    public ResponseDTO suspendProcess(@PathVariable String processId){
        return camundaProcessServices.suspendProcessImp(processId);
    }
//
    @RolesAllowed("admin")
    @PostMapping("/process/{processId}/activate")
    @Operation(summary = "Activate suspended process")
    public ResponseDTO activateProcess(@PathVariable String processId){
        return camundaProcessServices.activateProcessImp(processId);
    }
//
    @RolesAllowed({"admin","user"})
    @PostMapping("/process/{processInstanceId}/stop")
    @Operation(summary = "Stop process")
    public ResponseDTO stopProcessById(@PathVariable String processInstanceId){
        return camundaProcessServices.stopProcessByIdImp(processInstanceId);
    }

    @RolesAllowed({"admin","user"})
    @PostMapping("/process/{processInstanceId}/resume")
    @Operation(summary = "Resume stopped process")
    public ResponseDTO resumeProcessById(@PathVariable String processInstanceId){
        return camundaProcessServices.resumeProcessByIdImp(processInstanceId);
    }

    @RolesAllowed({"admin","user"})
    @PostMapping("/process/{processInstanceId}/variables")
    @Operation(summary = "Get process variables using process instance id")
    public RequestClaimProcessVariablesDTO getProcessVariables(@PathVariable String processInstanceId){
        return camundaProcessServices.getProcessVariablesImp(processInstanceId);
    }

    @PostMapping("/process/{processInstanceId}/task/current")
    @Operation(summary = "Get current process task")
    public String[] getCurrentTask(@PathVariable String processInstanceId){
        return camundaProcessServices.getCurrentTaskImp(processInstanceId);
    }



}
