package com.kaributechs.insurehubworkflowengineui.controllers;


import com.kaributechs.insurehubworkflowengineui.models.dtos.AddEvidenceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.DocumentDTO;
import com.kaributechs.insurehubworkflowengineui.services.IEvidenceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/v1/evidence")
@Tag(name = "Claims Evidence API endpoints", description = "Endpoints for the claims evidence only")
@CrossOrigin
public class EvidenceController {
    private final IEvidenceService documentsService;
    Logger logger = LoggerFactory.getLogger(this.getClass());

    public EvidenceController(IEvidenceService documentsService) {
        this.documentsService = documentsService;
    }

    @PostMapping("/add")
    @Operation(summary = "Add evidence to a claim using process id (claim id)")
    private List<DocumentDTO> addEvidence(@ModelAttribute AddEvidenceDTO addEvidenceDTO) throws IOException {
            logger.info(addEvidenceDTO.toString());
        return documentsService.addEvidenceImp(addEvidenceDTO);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get evidence file by evidence id")
    private DocumentDTO getEvidenceById(@PathVariable String id) throws IOException {
        return documentsService.getEvidenceByIdImp(id);
    }

    @GetMapping("/download/{id}")
    @Operation(summary = "Download evidence file by evidence id")
    public ResponseEntity<byte[]> downloadEvidence(@PathVariable String id) throws IOException {
        return documentsService.downloadEvidenceImp(id);
    }

    @GetMapping("/claim/{claimId}")
    @Operation(summary = "Get evidence files by process id (claim id)")
    private List<DocumentDTO> getEvidenceByClaimId(@PathVariable String claimId) throws IOException {
        return documentsService.getEvidenceByClaimIdImp(claimId);
    }

    @PostMapping("/delete/{id}")
    @Operation(summary = "Delete evidence file by evidence id")
    private void deleteEvidence(@PathVariable String id)  {
        documentsService.deleteEvidenceImp(id);
    }
}
