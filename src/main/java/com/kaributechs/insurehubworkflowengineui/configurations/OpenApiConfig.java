package com.kaributechs.insurehubworkflowengineui.configurations;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;

@OpenAPIDefinition(
        info = @Info(
                title = "InsureHub Camunda Workflow Engine",
                description = "" +
                        "This is an api documentation page for the InsureHub Workflow Engine developed by Kaributechs",
                contact = @Contact(
                        name = "Tinotenda Jobi",
                        email = "tinojobi@gmail.com"
                )
        ),
        servers = @Server(url = "http://insurehubbackend.southafricanorth.cloudapp.azure.com:6005/")
)
//@SecurityScheme(
//        name = "api",
//        scheme = "basic",
//        type = SecuritySchemeType.APIKEY,
//        in = SecuritySchemeIn.HEADER)
public class OpenApiConfig {
}
