package com.kaributechs.insurehubworkflowengineui.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class WebClientConfig {

    @Bean(name = "camundaWebClient")
    public WebClient camundaWebClient(){
//        String baseUrl = "http://localhost:6001/api/v1";
        String baseUrl = "http://camunda-engine:6001/api/v1";
        return WebClient.builder().baseUrl(baseUrl).build();
    }

    @Bean(name = "dbWebClient")
    public WebClient dbWebClient(){
//        String baseUrl = "http://localhost:6006/api/v1";
        String baseUrl = "http://db-engine:6006/api/v1";
        return WebClient.builder().baseUrl(baseUrl).build();
    }
}
