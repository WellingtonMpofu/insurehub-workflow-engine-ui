package com.kaributechs.insurehubworkflowengineui.aspects;

import com.kaributechs.insurehubworkflowengineui.models.dtos.claims.ClaimsWorkflowRequestDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.utilities.Utils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.context.annotation.Configuration;


@Configuration
@Aspect
public class LoggingAspect {
    Logger logger= LoggerFactory.getLogger(this.getClass());

    private final Utils utils;

    public LoggingAspect(Utils utils) {
        this.utils = utils;
    }

    @Before("com.kaributechs.insurehubworkflowengineui.aspects.CommonJoinPointConfig.requestClaim()")
    public void requestClaimBefore(JoinPoint joinPoint){
        ClaimsWorkflowRequestDTO claimsWorkflowRequestDTO= (ClaimsWorkflowRequestDTO) joinPoint.getArgs()[0];
        logger.info("Claim request by {} {}",claimsWorkflowRequestDTO.getClientFirstName(),claimsWorkflowRequestDTO.getClientLastName());
    }

    @AfterReturning(value = "com.kaributechs.insurehubworkflowengineui.aspects.CommonJoinPointConfig.requestClaim()",returning = "result")
    public void requestClaimAfter(JoinPoint joinPoint,ProcessInstanceDTO result) throws Throwable {
        logger.info("result : {}",result.getId());
        ClaimsWorkflowRequestDTO claimsWorkflowRequestDTO= (ClaimsWorkflowRequestDTO) joinPoint.getArgs()[0];
        String user=utils.getUsername();
        MDC.put("user",user);
        MDC.put("claim-id",result.getId());
        logger.info("Claim request by {} {} Successful.Claim id {}",claimsWorkflowRequestDTO.getClientFirstName(),claimsWorkflowRequestDTO.getClientLastName(),result.getId());
        MDC.remove("claim-id");

    }

//    @AfterReturning("com.kaributechs.insurehubcommunicate.aspects.CommonJoinPointConfig.sendEmailWithAttachments() || com.kaributechs.insurehubcommunicate.aspects.CommonJoinPointConfig.sendEmailWithoutAttachments()")
//    public void sendEmailSuccessful(JoinPoint joinPoint) throws MessagingException {
//
//        if (joinPoint.getArgs()[0].getClass().getSimpleName().equals("SmartMimeMessage")){
//            MimeMessage email = (MimeMessage) joinPoint.getArgs()[0];
//            String addresses= Arrays.stream(email.getAllRecipients()).map(Address::toString).collect(Collectors.joining(", "));
//            logger.info(String.format("Email sent to %s  ",addresses));
//
//        }else {
//            SimpleMailMessage email=(SimpleMailMessage) joinPoint.getArgs()[0];
//            String addresses= String.join(",",email.getTo());
//            logger.info(String.format("Email sent to %s  ",addresses));
//        }
//
//    }
//
//    @AfterThrowing(pointcut = "com.kaributechs.insurehubcommunicate.aspects.CommonJoinPointConfig.sendEmailWithAttachments() || com.kaributechs.insurehubcommunicate.aspects.CommonJoinPointConfig.sendEmailWithoutAttachments()",throwing = "ex")
//    public void sendEmailException(JoinPoint joinPoint, Exception ex) throws MessagingException {
//        if (joinPoint.getArgs()[0].getClass().getSimpleName().equals("SmartMimeMessage")){
//            MimeMessage email = (MimeMessage) joinPoint.getArgs()[0];
//            String addresses= Arrays.stream(email.getAllRecipients()).map(Address::toString).collect(Collectors.joining(", "));
//            logger.info(String.format("Something went wrong while sending email to %s : %s",addresses,ex.getMessage()));
//
//        }else {
//            SimpleMailMessage email=(SimpleMailMessage) joinPoint.getArgs()[0];
//            String addresses= String.join(",",email.getTo());
//            logger.info(String.format("Something went wrong while sending email to %s : %s",addresses,ex.getMessage()));
//        }
//    }





}
