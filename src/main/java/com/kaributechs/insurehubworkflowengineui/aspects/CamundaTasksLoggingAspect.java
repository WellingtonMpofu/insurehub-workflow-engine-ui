package com.kaributechs.insurehubworkflowengineui.aspects;

import com.kaributechs.insurehubworkflowengineui.models.dtos.TaskDTO;
import com.kaributechs.insurehubworkflowengineui.utilities.Utils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.context.annotation.Configuration;

@Configuration
@Aspect
public class CamundaTasksLoggingAspect {
    Logger logger= LoggerFactory.getLogger(this.getClass());
    private final Utils utils;

    public CamundaTasksLoggingAspect(Utils utils) {
        this.utils = utils;
    }

    @Before("com.kaributechs.insurehubworkflowengineui.aspects.CommonJoinPointConfig.completeTask()")
    public void completeTaskBefore(JoinPoint joinPoint){
        String taskId= (String) joinPoint.getArgs()[0];
        logger.info("{} completing task with id {}",utils.getUsername(),taskId);
    }

    @AfterReturning(value = "com.kaributechs.insurehubworkflowengineui.aspects.CommonJoinPointConfig.completeTask()",returning = "taskDTO")
    public void requestClaimAfter(JoinPoint joinPoint, TaskDTO taskDTO) throws Throwable {

        String taskId= (String) joinPoint.getArgs()[0];
        String user=utils.getUsername();
        MDC.put("user",user);
        MDC.put("task-id",taskId);
        logger.info("{} completed task with id {} successfully",user,taskId);
        MDC.remove("task-id");

    }
}
