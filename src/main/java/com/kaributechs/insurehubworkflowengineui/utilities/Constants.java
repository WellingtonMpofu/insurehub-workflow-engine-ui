package com.kaributechs.insurehubworkflowengineui.utilities;

public final class Constants {
    public static final String activitiEngineBaseUrl="http://localhost:8081/api/v1";
//    public static final String camundaEngineBaseUrl="http://localhost:6001/api/v1";
    public static final String camundaEngineBaseUrl="http://camunda-engine:6001/api/v1";
}
