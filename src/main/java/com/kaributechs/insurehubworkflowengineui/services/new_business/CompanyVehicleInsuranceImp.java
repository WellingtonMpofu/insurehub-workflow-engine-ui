package com.kaributechs.insurehubworkflowengineui.services.new_business;


import com.kaributechs.insurehubworkflowengineui.exceptions.GenericException;
import com.kaributechs.insurehubworkflowengineui.models.dtos.claims.ClaimsWorkflowRequestDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.company_vehicle_insurance.CompanyVehicleInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.company_vehicle_insurance.CompanyVehicleInsuranceDTO;
import com.kaributechs.insurehubworkflowengineui.utilities.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Objects;

import static org.springframework.http.MediaType.APPLICATION_JSON;

@Service
@Slf4j
public class CompanyVehicleInsuranceImp implements ICompanyVehicleInsuranceService {
    private static final String AUTHORIZATION="Authorization";
    private static final String BEARER="Bearer ";
    @Qualifier("camundaWebClient")
    private final WebClient camundaWebClient;
    private final Utils utils;
    @Qualifier("dbWebClient")
    private final WebClient dbWebClient;

    public CompanyVehicleInsuranceImp(WebClient camundaWebClient, Utils utils, WebClient dbWebClient) {
        this.camundaWebClient = camundaWebClient;
        this.utils = utils;
        this.dbWebClient = dbWebClient;
    }

    @Override
    public ProcessInstanceDTO apply(CompanyVehicleInsuranceDTO companyVehicleInsuranceDTO) {
        log.info("Start CompanyVehicleInsurance process : {}",companyVehicleInsuranceDTO);

        ProcessInstanceDTO processInstanceDTO=sendCompanyVehicleInsuranceDataToCamundaEngine(companyVehicleInsuranceDTO);

        if (processInstanceDTO!=null){
            companyVehicleInsuranceDTO.setProcessId(processInstanceDTO.getId());

            saveCompanyVehicleInsuranceDataToDBEngine(companyVehicleInsuranceDTO);

        }
        log.info("company vehicle insurance process created : {}",processInstanceDTO);
        return  processInstanceDTO;
    }

    @Override
    public CompanyVehicleInsurance getInsuranceByProcessId(String processId) {
        try {
            Mono<CompanyVehicleInsurance> response = dbWebClient.get().uri("/company-vehicle-insurance/get/{processId}", processId).accept(APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION,AUTHORIZATION, BEARER+utils.getToken())
                    .exchangeToMono(clientResponse -> {
                                if (clientResponse.statusCode().isError()) {
                                    return null;
                                }
                                return clientResponse.bodyToMono(CompanyVehicleInsurance.class);
                            }
                    );
            return response.block();

        }catch (Exception e){
            throw new GenericException(e.getMessage());
        }
    }

    public ProcessInstanceDTO sendCompanyVehicleInsuranceDataToCamundaEngine(CompanyVehicleInsuranceDTO companyVehicleInsuranceDTO) {
        try {
            Mono<ProcessInstanceDTO> response = camundaWebClient.post().uri("/company-vehicle-insurance/apply")
                    .accept(MediaType.APPLICATION_JSON).body(Mono.just(companyVehicleInsuranceDTO), ClaimsWorkflowRequestDTO.class)
                    .header(HttpHeaders.AUTHORIZATION,"Authorization", "Bearer "+utils.getToken())
                    .retrieve()
                    .bodyToMono(ProcessInstanceDTO.class);

            return response.block();
        } catch (Exception e){
            log.error("Error requesting company vehicle insurance");
            throw new GenericException("Error From Camunda Engine :"+ e.getMessage());
        }

    }

    public void saveCompanyVehicleInsuranceDataToDBEngine(CompanyVehicleInsuranceDTO companyVehicleInsuranceDTO) {
        try {
            Mono<ClientResponse> response = dbWebClient.post().uri("/company-vehicle-insurance/apply").accept(MediaType.APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION, "Authorization", "Bearer " + utils.getToken())
                    .body(BodyInserters.fromPublisher(Mono.just(companyVehicleInsuranceDTO), CompanyVehicleInsuranceDTO.class))
                    .exchange();
            HttpStatus status = Objects.requireNonNull(response.block()).statusCode();


            if (!status.is2xxSuccessful()) {
                throw new GenericException("Error Creating company vehicle insurance : Status code " + status.value());
            }


        } catch (Exception e) {
            throw new GenericException("Error Creating CompanyVehicleInsuranceData :" + e.getMessage());
        }

    }


}
