package com.kaributechs.insurehubworkflowengineui.services.new_business;

import com.kaributechs.insurehubworkflowengineui.exceptions.GenericException;
import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.household_insurance.HouseholdInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.household_insurance.HouseholdInsuranceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.life_insurance.LifeInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.life_insurance.LifeInsuranceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.personal_vehicle_insurance.PersonalVehicleInsurance;
import com.kaributechs.insurehubworkflowengineui.utilities.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Objects;

import static org.springframework.http.MediaType.APPLICATION_JSON;

@Service
@Slf4j
public class HouseholdInsuranceServiceImp implements IHouseholdInsuranceService{
    private static final String AUTHORIZATION="Authorization";
    private static final String BEARER="Bearer ";
    @Qualifier("camundaWebClient")
    private final WebClient camundaWebClient;
    private final Utils utils;
    @Qualifier("dbWebClient")
    private final WebClient dbWebClient;

    public HouseholdInsuranceServiceImp(WebClient camundaWebClient, Utils utils, WebClient dbWebClient) {
        this.camundaWebClient = camundaWebClient;
        this.utils = utils;
        this.dbWebClient = dbWebClient;
    }

    @Override
    public ProcessInstanceDTO apply(HouseholdInsuranceDTO householdInsuranceDTO) {
        log.info("Start Household Insurance process : {}",householdInsuranceDTO);
        ProcessInstanceDTO processInstanceDTO= sendInsuranceDataToCamundaEngine(householdInsuranceDTO);

        if (processInstanceDTO!=null){
            householdInsuranceDTO.setProcessId(processInstanceDTO.getId());

            saveInsuranceDataToDBEngine(householdInsuranceDTO);

        }
        log.info("household insurance process created : {}",processInstanceDTO);
        return  processInstanceDTO;
    }

    @Override
    public HouseholdInsurance getInsuranceByProcessId(String processId) {
        try {
            Mono<HouseholdInsurance> response = dbWebClient.get().uri("/household-insurance/get/{processId}", processId).accept(APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION,AUTHORIZATION, BEARER+utils.getToken())
                    .retrieve().bodyToMono(HouseholdInsurance.class);
            return response.block();
        }catch (Exception e){
            throw new GenericException(e.getMessage());
        }
    }

    public ProcessInstanceDTO sendInsuranceDataToCamundaEngine(HouseholdInsuranceDTO householdInsuranceDTO) {
        try {
            Mono<ProcessInstanceDTO> response = camundaWebClient.post().uri("/household-insurance/apply")
                    .accept(MediaType.APPLICATION_JSON).body(Mono.just(householdInsuranceDTO), HouseholdInsuranceDTO.class)
                    .header(HttpHeaders.AUTHORIZATION,"Authorization", "Bearer "+utils.getToken())
                    .retrieve()
                    .bodyToMono(ProcessInstanceDTO.class);
            return response.block();
        } catch (Exception e){
            log.error("Error requesting household insurance");
            throw new GenericException("Error From Camunda Engine :"+ e.getMessage());
        }
    }

    public void saveInsuranceDataToDBEngine(HouseholdInsuranceDTO householdInsuranceDTO) {
        try {
            Mono<ClientResponse> response = dbWebClient.post().uri("/household-insurance/save").accept(MediaType.APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION, "Authorization", "Bearer " + utils.getToken())
                    .body(BodyInserters.fromPublisher(Mono.just(householdInsuranceDTO), HouseholdInsuranceDTO.class))
                    .exchange();
            HttpStatus status = Objects.requireNonNull(response.block()).statusCode();
            if (!status.is2xxSuccessful()) {
                throw new GenericException("Error creating household insurance : Status code " + status.value());
            }
        } catch (Exception e) {
            throw new GenericException("Error creating household insurance :" + e.getMessage());
        }

    }


}
