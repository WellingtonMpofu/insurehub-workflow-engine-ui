package com.kaributechs.insurehubworkflowengineui.services.dealer.implementation;

import com.kaributechs.insurehubworkflowengineui.exceptions.GenericException;
import com.kaributechs.insurehubworkflowengineui.models.dtos.RegisterDealerDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.dealer.AddDealerServiceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.dealer.DealerDTO;
import com.kaributechs.insurehubworkflowengineui.services.dealer.IDealerService;
import com.kaributechs.insurehubworkflowengineui.utilities.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class DealerServiceImp implements IDealerService {
    private final Utils utils;
    @Qualifier("dbWebClient")
    private final WebClient dbWebClient;

    public DealerServiceImp(Utils utils, WebClient dbWebClient) {
        this.utils = utils;
        this.dbWebClient = dbWebClient;
    }

    @Override
    public DealerDTO registerDealer(RegisterDealerDTO registerDealerDTO) {
        String username=utils.getUsername();
        log.info("Registering Dealer : {}",username);

        try {
            Mono<DealerDTO> response = dbWebClient.post().uri("/dealer/register")
                    .accept(MediaType.APPLICATION_JSON)
                    .body(Mono.just(registerDealerDTO), RegisterDealerDTO.class)
                    .header(HttpHeaders.AUTHORIZATION,"Authorization", "Bearer "+utils.getToken())
                    .retrieve()
                    .bodyToMono(DealerDTO.class);

            return response.block();
        } catch (Exception e) {
            throw new GenericException("Error registering dealer :" + e.getMessage());
        }
    }

    @Override
    public DealerDTO addDealerService(AddDealerServiceDTO addDealerServiceDTO) {
        log.info("Adding service {}", addDealerServiceDTO);
        try {
            Mono<DealerDTO> response = dbWebClient.post().uri("/dealer/service/add")
                    .accept(MediaType.APPLICATION_JSON)
                    .body(Mono.just(addDealerServiceDTO), AddDealerServiceDTO.class)
                    .header(HttpHeaders.AUTHORIZATION,"Authorization", "Bearer "+utils.getToken())
                    .retrieve()
                    .bodyToMono(DealerDTO.class);

            return response.block();
        } catch (Exception e){
            log.error("TError adding service");
            throw new GenericException("Error From DB Engine :"+ e.getMessage());
        }
    }

    @Override
    public DealerDTO getDealerByUsername(String username) {

        try {
            Mono<DealerDTO> response = dbWebClient.get().uri("/dealer/{username}", username).accept(MediaType.APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION,"Authorization", "Bearer "+utils.getToken())
                    .retrieve()
                    .bodyToMono(DealerDTO.class);

            return response.block();
        }catch (Exception e){
            throw new GenericException("Error DB Camunda Engine :"+ e.getMessage());
        }


    }
}
