package com.kaributechs.insurehubworkflowengineui.services.dealer;


import com.kaributechs.insurehubworkflowengineui.models.dtos.RegisterDealerDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.dealer.AddDealerServiceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.dealer.DealerDTO;

public interface IDealerService {

    DealerDTO registerDealer(RegisterDealerDTO registerDealerDTO);

    DealerDTO addDealerService(AddDealerServiceDTO dealerServiceDTO);

    DealerDTO getDealerByUsername(String username);
}
