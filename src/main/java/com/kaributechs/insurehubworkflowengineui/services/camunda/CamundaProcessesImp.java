package com.kaributechs.insurehubworkflowengineui.services.camunda;

import com.kaributechs.insurehubworkflowengineui.exceptions.GenericException;
import com.kaributechs.insurehubworkflowengineui.models.dtos.*;
import com.kaributechs.insurehubworkflowengineui.models.dtos.claims.*;
import com.kaributechs.insurehubworkflowengineui.services.ICamundaProcessServices;
import com.kaributechs.insurehubworkflowengineui.services.IEvidenceService;
import com.kaributechs.insurehubworkflowengineui.utilities.Utils;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON;

@Service
@Slf4j
public class CamundaProcessesImp implements ICamundaProcessServices {
    private static final String AUTHORIZATION="Authorization";
    private static final String BEARER="Bearer ";


    Logger logger = LoggerFactory.getLogger(this.getClass());


    @Qualifier("camundaWebClient")
    private final WebClient camundaWebClient;
    private final Utils utils;


    public CamundaProcessesImp(WebClient camundaWebClient,Utils utils) {
        this.camundaWebClient = camundaWebClient;
        this.utils = utils;
    }


    @Override
    public List<ProcessDefinitionDTO> getAllProcessesImp() {
        logger.info("Get All Processes");
        try {
            Mono<ProcessDefinitionDTO[]> response = camundaWebClient.get().uri("/processes").accept(APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION,
                            AUTHORIZATION, BEARER+utils.getToken())
                    .retrieve()
                    .bodyToMono(ProcessDefinitionDTO[].class);

            ProcessDefinitionDTO[] processes = response.block();
            assert processes != null;
            List<ProcessDefinitionDTO> processDefinitionDTOList =Arrays.asList(processes.clone());
            logger.info("Processes : {}",processDefinitionDTOList);

            return processDefinitionDTOList;
        }catch (Exception e){
            throw new GenericException(e.getMessage());
        }

    }



    @Override
    public List<ProcessInstanceDTO> getRunningProcessesImp() {
        logger.info("Get running processes");
        try {
            Mono<ProcessInstanceDTO[]> response = camundaWebClient.get().uri("/processes/running").accept(APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION,AUTHORIZATION, BEARER+utils.getToken())
                    .retrieve()
                    .bodyToMono(ProcessInstanceDTO[].class);

            ProcessInstanceDTO[] processes = response.block();
            assert processes != null;
            return Arrays.asList(processes.clone());
        }catch (Exception e){
            throw new GenericException(e.getMessage());
        }
    }

    @Override
    public ResponseDTO suspendProcessImp(String processId) {
        try {
            Mono<ResponseDTO> response = camundaWebClient.post().uri("/process/{processId}/suspend", processId).accept(APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION,AUTHORIZATION, BEARER+utils.getToken())
                    .exchangeToMono(clientResponse -> {
                                if (clientResponse.statusCode().isError()) {
                                    return null;
                                }
                                return clientResponse.bodyToMono(ResponseDTO.class);
                            }
                    );

            return response.block();
        }catch (Exception e){
            throw new GenericException(e.getMessage());
        }


    }

    @Override
    public ResponseDTO activateProcessImp(String processId) {
    try {
        Mono<ResponseDTO> response = camundaWebClient.post().uri("/process/{processId}/activate", processId).accept(APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION,AUTHORIZATION, BEARER+utils.getToken())
                .exchangeToMono(clientResponse -> {
                            if (clientResponse.statusCode().isError()) {
                                return null;
                            }
                            return clientResponse.bodyToMono(ResponseDTO.class);
                        }
                );

        return response.block();
    }catch (Exception e){
        throw new GenericException(e.getMessage());
    }

    }

    @Override
    public ResponseDTO stopProcessByIdImp(String processInstanceId) {
        try {
            Mono<ResponseDTO> response = camundaWebClient.post().uri("/process/{processInstanceId}/stop", processInstanceId).accept(APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION,AUTHORIZATION, BEARER+utils.getToken())
                    .exchangeToMono(clientResponse -> {
                                if (clientResponse.statusCode().isError()) {
                                    return null;
                                }
                                return clientResponse.bodyToMono(ResponseDTO.class);
                            }
                    );

            return response.block();
        }catch (Exception e){
            throw new GenericException(e.getMessage());
        }

    }

    @Override
    public ResponseDTO resumeProcessByIdImp(String processInstanceId) {
        try {
            Mono<ResponseDTO> response = camundaWebClient.post().uri("/process/{processInstanceId}/resume", processInstanceId).accept(APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION,AUTHORIZATION, BEARER+utils.getToken())
                    .exchangeToMono(clientResponse -> {
                                if (clientResponse.statusCode().isError()) {
                                    return null;
                                }
                                return clientResponse.bodyToMono(ResponseDTO.class);
                            }
                    );

            return response.block();
        }catch (Exception e){
            throw new GenericException(e.getMessage());
        }


    }

    @Override
    public RequestClaimProcessVariablesDTO getProcessVariablesImp(String processInstanceId) {
        logger.info("Get process with id {} variables",processInstanceId);
        try {
            Mono<RequestClaimProcessVariablesDTO> response = camundaWebClient.post().uri("/process/{processInstanceId}/variables", processInstanceId).accept(APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION,AUTHORIZATION, BEARER+utils.getToken())
                    .exchangeToMono(clientResponse -> {
                                if (clientResponse.statusCode().isError()) {
                                    return null;
                                }
                                return clientResponse.bodyToMono(RequestClaimProcessVariablesDTO.class);
                            }
                    );
            RequestClaimProcessVariablesDTO processVariables=response.block();
            logger.info("process {} variabes : {}",processInstanceId,processVariables);
            return processVariables;

        }catch (Exception e){
            throw new GenericException(e.getMessage());
        }
    }

    @Override
    public String[] getCurrentTaskImp(String processInstanceId) {
        try {
            Mono<String[]> response = camundaWebClient.post().uri("/process/{processInstanceId}/task/current", processInstanceId).accept(APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION,AUTHORIZATION, BEARER+utils.getToken())
                    .exchangeToMono(clientResponse -> {
                                if (clientResponse.statusCode().isError()) {
                                    return null;
                                }
                                return clientResponse.bodyToMono(String[].class);
                            }
                    );

            return response.block();
        }catch (Exception e){
            throw new GenericException(e.getMessage());
        }    }



}
