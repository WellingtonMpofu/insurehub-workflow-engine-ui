package com.kaributechs.insurehubworkflowengineui.services.camunda;

import com.kaributechs.insurehubworkflowengineui.exceptions.GenericException;
import com.kaributechs.insurehubworkflowengineui.models.dtos.*;
import com.kaributechs.insurehubworkflowengineui.models.dtos.claims.CompleteClaimTaskDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.claims.RequestClaimProcessVariablesDTO;
import com.kaributechs.insurehubworkflowengineui.services.ICamundaTaskServices;
import com.kaributechs.insurehubworkflowengineui.utilities.Utils;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class CamundaTasksImp implements ICamundaTaskServices {

    Logger logger = LoggerFactory.getLogger(this.getClass());


    @Qualifier("camundaWebClient")
    private final WebClient camundaWebClient;
    private final Utils utils;

    public CamundaTasksImp(WebClient camundaWebClient, Utils utils) {
        this.camundaWebClient = camundaWebClient;
        this.utils = utils;
    }

    @Override
    public List<TaskDTO> getProcessTasksImp(String processInstanceId) {
        try{
            Mono<TaskDTO[]> response = camundaWebClient.get().uri("/process/{processInstanceId}/tasks", processInstanceId).accept(MediaType.APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION,"Authorization", "Bearer "+utils.getToken())
                    .retrieve()
                    .bodyToMono(TaskDTO[].class);

            TaskDTO[] tasks = response.block();
            assert tasks != null;
            return Arrays.asList(tasks.clone());
        }catch (Exception e){
            throw new GenericException("Error From Camunda Engine :"+ e.getMessage());
        }

    }

    @Override
    public TaskDTO completeTask(String taskId, CompleteClaimTaskDTO completeClaimTaskDTO) {
        try {
            Mono<TaskDTO> response = camundaWebClient.post().uri("/task/{taskId}/complete", taskId)
                    .accept(MediaType.APPLICATION_JSON)
                    .body(Mono.just(completeClaimTaskDTO), CompleteClaimTaskDTO.class)
                    .header(HttpHeaders.AUTHORIZATION,"Authorization", "Bearer "+utils.getToken())
                    .retrieve()
                    .bodyToMono(TaskDTO.class);

            return response.block();
        } catch (Exception e){
            logger.error("Task with id {} not found",taskId);
            throw new GenericException("Error From Camunda Engine :"+ e.getMessage());
        }

    }

    @Override
    public TaskResponseDTO getTaskById(String taskId) {
        try {
            Mono<TaskResponseDTO> response = camundaWebClient.get().uri("/task/{taskId}", taskId).accept(MediaType.APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION,"Authorization", "Bearer "+utils.getToken())
                    .retrieve()
                    .bodyToMono(TaskResponseDTO.class);

            return response.block();
        }catch (Exception e){
            throw new GenericException("Error From Camunda Engine :"+ e.getMessage());
        }

    }

    @Override
    public List<TaskDTO> getTasksByUsernameImp(String username) {
        try {
            Mono<TaskDTO[]> response = camundaWebClient.get().uri("/{username}/tasks", username).accept(MediaType.APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION,"Authorization", "Bearer "+utils.getToken())
                    .retrieve()
                    .bodyToMono(TaskDTO[].class);

            TaskDTO[] tasks = response.block();
            assert tasks != null;
            return Arrays.asList(tasks.clone());
        }catch (Exception e){
            throw new GenericException("Error From Camunda Engine :"+ e.getMessage());
        }

    }

    @Override
    public ResponseDTO assignTask(AssignTaskDTO assignTaskDTO) {
        try {
            Mono<ResponseDTO> response = camundaWebClient.post().uri("/task/assign")
                    .accept(MediaType.APPLICATION_JSON).body(Mono.just(assignTaskDTO), AssignTaskDTO.class)
                    .header(HttpHeaders.AUTHORIZATION,"Authorization", "Bearer "+utils.getToken())
                    .retrieve()
                    .bodyToMono(ResponseDTO.class);

            return response.block();
        }catch (Exception e){
            throw new GenericException("Error From Camunda Engine :"+ e.getMessage());
        }

    }

    @Override
    public ResponseDTO claimTask(String taskId) {

        try {
            Mono<ResponseDTO> response = camundaWebClient.post().uri("/{username}/{taskId}/claim",utils.getUsername(), taskId).accept(MediaType.APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION,"Authorization", "Bearer "+utils.getToken())
                    .retrieve()
                    .bodyToMono(ResponseDTO.class);

            return response.block();
        }catch (Exception e){
            log.info("Exception : {}",e.toString());
            throw new GenericException("Error From Camunda Engine :"+ e.getMessage());
        }


    }

    @Override
    public List<TaskDTO> getMyTasksImp() {
        return getTasksByUsernameImp(utils.getUsername());
    }

    @Override
    public List<TaskDTO> getTeamTasksImp() {
        try {
            Mono<TaskDTO[]> response = camundaWebClient.get().uri("/team/tasks").accept(MediaType.APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION,"Authorization", "Bearer "+utils.getToken())
                    .retrieve()
                    .bodyToMono(TaskDTO[].class);

            TaskDTO[] tasks = response.block();
            assert tasks != null;
            return Arrays.asList(tasks.clone());
        }catch (Exception e){
            throw new GenericException("Error From Camunda Engine :"+ e.getMessage());
        }

    }
}
