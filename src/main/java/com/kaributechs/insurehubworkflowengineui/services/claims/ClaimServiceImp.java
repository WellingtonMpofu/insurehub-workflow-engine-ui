package com.kaributechs.insurehubworkflowengineui.services.claims;

import com.kaributechs.insurehubworkflowengineui.exceptions.GenericException;
import com.kaributechs.insurehubworkflowengineui.models.dtos.EvidenceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.claims.*;
import com.kaributechs.insurehubworkflowengineui.models.dtos.DocumentDTO;
import com.kaributechs.insurehubworkflowengineui.services.IEvidenceService;
import com.kaributechs.insurehubworkflowengineui.utilities.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
public class ClaimServiceImp implements IClaimService{

    private final Utils utils;
    @Qualifier("dbWebClient")
    private final WebClient dbWebClient;
    private final IEvidenceService documentsService;
    private final IEvidenceService evidenceService;




    public ClaimServiceImp(Utils utils, WebClient dbWebClient, IEvidenceService documentsService, IEvidenceService evidenceService) {
        this.utils = utils;
        this.dbWebClient = dbWebClient;
        this.documentsService = documentsService;
        this.evidenceService = evidenceService;
    }

    @Override
    public ProcessInstanceDTO startClaimsProcess(ClaimsWorkflowRequestDTO claimsWorkflowRequestDTO) throws IOException {
        log.info("Start claims process : {}",claimsWorkflowRequestDTO);

        MultipartFile[] policeReport= claimsWorkflowRequestDTO.getPoliceReport();
        MultipartFile[] videoEvidence=claimsWorkflowRequestDTO.getVideoEvidence();
        MultipartFile[] audioEvidence=claimsWorkflowRequestDTO.getAudioEvidence();
        MultipartFile[] additionalDocuments=claimsWorkflowRequestDTO.getAdditionalDocuments();

        claimsWorkflowRequestDTO.setClaimCreationDate(new Date());
        claimsWorkflowRequestDTO.setPoliceReport(null);
        claimsWorkflowRequestDTO.setAdditionalDocuments(null);
        claimsWorkflowRequestDTO.setAudioEvidence(null);
        claimsWorkflowRequestDTO.setVideoEvidence(null);

        ProcessInstanceDTO processInstanceDTO=utils.sendClaimDataToCamundaEngine(mapClaimData(claimsWorkflowRequestDTO));

        if (processInstanceDTO!=null){
            ClaimDTO claimDTO =new ClaimDTO();
            claimDTO.setProcessInstanceId(processInstanceDTO.getId());
            claimDTO.setStatus("CREATED");
            claimDTO.setUsername(utils.getUsername());

            claimDTO.setClaimsWorkflowRequestDTO(mapClaimData(claimsWorkflowRequestDTO));
            utils.saveClaimToDBEngine(claimDTO);


            if(policeReport!=null && policeReport.length>0){
                evidenceService.uploadEvidenceImp(mapEvidenceData(claimsWorkflowRequestDTO,"Police Report", processInstanceDTO.getId(), policeReport));
            }
            if(videoEvidence!=null && videoEvidence.length>0){
                evidenceService.uploadEvidenceImp(mapEvidenceData(claimsWorkflowRequestDTO,"Video Evidence", processInstanceDTO.getId(), videoEvidence));
            }
            if(audioEvidence!=null && audioEvidence.length>0){
                evidenceService.uploadEvidenceImp(mapEvidenceData(claimsWorkflowRequestDTO,"Audio Evidence", processInstanceDTO.getId(), audioEvidence));
            }
            if(additionalDocuments!=null && additionalDocuments.length>0){
                evidenceService.uploadEvidenceImp(mapEvidenceData(claimsWorkflowRequestDTO,"Additional Documents", processInstanceDTO.getId(), additionalDocuments));
            }
        }
        log.info("Claims process created : {}",processInstanceDTO);
        return  processInstanceDTO;

    }

    private EvidenceDTO mapEvidenceData(ClaimsWorkflowRequestDTO claimsWorkflowRequestDTO, String category, String processId, MultipartFile[] files){
        EvidenceDTO evidenceDTO=new EvidenceDTO();
        evidenceDTO.setProcessId(processId);
        evidenceDTO.setClientId(claimsWorkflowRequestDTO.getClientId());
        evidenceDTO.setClientFirstName(claimsWorkflowRequestDTO.getClientFirstName());
        evidenceDTO.setClientLastName(claimsWorkflowRequestDTO.getClientLastName());
        evidenceDTO.setFiles(files);
        evidenceDTO.setClientIdNumber(claimsWorkflowRequestDTO.getClientIdNumber());
        evidenceDTO.setDocumentSourceChannel(claimsWorkflowRequestDTO.getDocumentSourceChannel());
        evidenceDTO.setBusinessName(claimsWorkflowRequestDTO.getBusinessName());
        evidenceDTO.setBusinessRegistrationNumber(claimsWorkflowRequestDTO.getBusinessRegistrationNumber());
        evidenceDTO.setCategory(category);
        evidenceDTO.setClientType(claimsWorkflowRequestDTO.getClientType());
        return evidenceDTO;
    }

    private DBWorkflowRequestDTO mapClaimData(ClaimsWorkflowRequestDTO claimsWorkflowRequestDTO){
        DBWorkflowRequestDTO dbWorkflowRequestDTO=new DBWorkflowRequestDTO();
        BeanUtils.copyProperties(claimsWorkflowRequestDTO,dbWorkflowRequestDTO);

        ClaimClientDetailsDTO clientDetails= new ClaimClientDetailsDTO();
        BeanUtils.copyProperties(claimsWorkflowRequestDTO,clientDetails);
        dbWorkflowRequestDTO.setClientDetails(clientDetails);

        ClaimInsuredDetailsDTO insuredDetails=new ClaimInsuredDetailsDTO();
        BeanUtils.copyProperties(claimsWorkflowRequestDTO,insuredDetails);
        dbWorkflowRequestDTO.setInsuredDetails(insuredDetails);

        ClaimVehicleDetailsDTO vehicleDetails=new ClaimVehicleDetailsDTO();
        BeanUtils.copyProperties(claimsWorkflowRequestDTO,vehicleDetails);
        dbWorkflowRequestDTO.setVehicleDetails(vehicleDetails);

        ClaimDriverDetailsDTO driverDetails=new ClaimDriverDetailsDTO();
        BeanUtils.copyProperties(claimsWorkflowRequestDTO,driverDetails);
        dbWorkflowRequestDTO.setDriverDetails(driverDetails);

        ClaimAccidentDetailsDTO accidentDetails=new ClaimAccidentDetailsDTO();
        BeanUtils.copyProperties(claimsWorkflowRequestDTO,accidentDetails);
        dbWorkflowRequestDTO.setAccidentDetails(accidentDetails);

        log.info("DBWorkflowRequestDTO : {}",claimsWorkflowRequestDTO);

        return dbWorkflowRequestDTO;
    }



    @Override
    public List<Claim> getMyClaimsImp() {
        try {
            Mono<Claim[]> response = dbWebClient.get().uri("/claims/{username}",utils.getUsername()).accept(MediaType.APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION,"Authorization", "Bearer "+utils.getToken())
                    .exchangeToMono(clientResponse -> {
                                if (clientResponse.statusCode().isError()) {
                                    return null;
                                }
                                return clientResponse.bodyToMono(Claim[].class);
                            }
                    );
            return Arrays.asList(Objects.requireNonNull(response.block()).clone());
        } catch (Exception e){
            throw  new GenericException("Error from Claims Engine : "+e.getMessage());
        }

    }

    @Override
    public List<DocumentDTO> getEvidence(String processId) throws IOException {
        return documentsService.getEvidenceByClaimIdImp(processId);
    }
}
