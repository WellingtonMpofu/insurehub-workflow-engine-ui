package com.kaributechs.insurehubworkflowengineui.services.claims;

import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.claims.Claim;
import com.kaributechs.insurehubworkflowengineui.models.dtos.DocumentDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.claims.ClaimsWorkflowRequestDTO;

import java.io.IOException;
import java.util.List;

public interface IClaimService {
    ProcessInstanceDTO startClaimsProcess(ClaimsWorkflowRequestDTO claimsWorkflowRequestDTO) throws IOException;

    List<Claim> getMyClaimsImp();

    List<DocumentDTO> getEvidence(String processId) throws IOException;
}
